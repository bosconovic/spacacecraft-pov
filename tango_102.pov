//* Generated on : 01-Feb-2020 12:36:15

#version 3.7;

global_settings {
    assumed_gamma 1.5
    noise_generator 2
}

#include "satellite.inc"
#include "earth.inc"


camera {
   perspective
   location <11204.5036,-1975.655,7047.528>
   look_at <11204.5025,-1975.6561,7047.5202>
   right <1.6,0,0>
   angle 31.7187
}

object {Earth(173.3284,true,true,false,1.0)}

object {
    spacecraft
    scale <0.001,0.001,0.001>
    matrix <0.73131,-0.38406,-0.56363,
            0.26898,-0.597,0.75581,
            -0.62676,-0.70434,-0.33329,
            11204.5025,-1975.6561,7047.5202>
}

light_source {
   <0,0,0>, rgb <1,1,1>
   area_light
   <696340,0,0>, <0,696340,0>, 32, 32
   adaptive 0
   jitter
   circular
   orient
   looks_like {
      sphere { <0,0,0>, 696340
         texture {
            pigment {color rgb <1,1,1> }
            finish { ambient 1 }
         }
      }
   }
   translate <1.4723e8,0,0>
}
