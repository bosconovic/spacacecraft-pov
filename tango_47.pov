//* Generated on : 03-Feb-2020 15:06:35

#version 3.7;

global_settings {
    assumed_gamma 1.5
    noise_generator 2
}

#include "satellite.inc"
#include "earth.inc"


camera {
   perspective
   location <425.4061,2923.1568,-7295.0591>
   look_at <425.3994,2923.1494,-7295.0587>
   right <1.6,0,0>
   angle 31.7187
}

object {Earth(155.3944,true,true,false,1.0)}

object {
    spacecraft
    scale <0.001,0.001,0.001>
    matrix <0.3143,-0.936,-0.15847,
            0.27235,0.24881,-0.92947,
            0.90942,0.24898,0.33312,
            425.3994,2923.1494,-7295.0587>
}

light_source {
   <1.4723e8,0,0>, rgb<1,1,1>
   area_light
   <696340,0,0>, <0,696340,0>, 32, 32
   adaptive 0
   jitter
   circular
   orient
}

