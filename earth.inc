// earth.inc by Scott Gammans http://www.scottgammans.com
// adapted by Francescodario Cuzzocrea and Jacopo Guarneri

// This file uses three images of the earth, all mercator projections:
// This file uses three images of the earth, all mercator projections:
// (1) earth_landmask_mercator.tif - black & white landmask where
//     water is black and land is white
// (2) earth_truecolor_mercator.tif - truecolor image of earth
// (3) earth_clouds_mercator.tif - black & white cloud layer image
// All of these files can be obtained at NASA's Visible Earth website
// http://visibleearth.nasa.gov/

// Earth radius in kilometer
#declare EARTH_RADIUS=6378;

// Example usage: object {Earth(0,true,true,true,1.0)}
// where
// axial_rot =  Earth's axial rotation in degrees
// use_spec =   if true, use specular water feature
// use_clouds = if true, use cloud layer
// use_diff =   if true, use atmospheric diffusion
// diff_adj =   should normally be 1.0. Amts above/below 1
//              thicken/reduce the blue ring of diffusion
//              around planet. Useful for when rendering a
//              small image and you want the ring to show up
#macro Earth(axial_rot,use_spec,use_clouds,use_diff,diff_adj)
union {

    // The base planet:
    sphere {0,EARTH_RADIUS
        texture {
            #if (use_spec=true)
             // This image file is a two-color mercator-projection image of
             // the Earth where the water is black and the land is white
             image_pattern {tiff "textures/earth_landmask_mercator.tif" map_type 1}
             texture_map {
                 // This is a true-color mercator image of the Earth
                 [0.0000 pigment{
                             image_map {tiff "textures/earth_truecolor_mercator.tif" map_type 1}
                         }
                         finish {
                             ambient 0.0
                             specular 0.10       // 0.0
                             reflection {0.05,0.25} //
                             roughness 0.10     // 0.05
                             diffuse 0.85        // 0.6
                             brilliance 1      // 1.0
                             phong 0.025         // 0.0
                             phong_size 75       // 40    < less glossy
                         }
                 ]
                 // This is a true-color mercator image of the Earth
                 [1.0000 pigment{
                             image_map {tiff "textures/earth_truecolor_mercator.tif" map_type 1}
                         }
                         finish {ambient 0.0}
                 ]
             }
                scale EARTH_RADIUS
            #else
               // This is a true-color mercator image of the Earth
                pigment{image_map {tiff "textures/earth_truecolor_mercator.tif" map_type 1}}
                finish {ambient 0.0}
            #end
        }
    }

    // The cloud layer:
    #if (use_clouds=true)
        difference {
            sphere {0,EARTH_RADIUS*1.0002}
            sphere {0,EARTH_RADIUS*1.0001}
            texture {
             pigment {
              // This is a two-color mercator image of Earth's cloud layer
              image_pattern {tiff "textures/earth_clouds_mercator.tif" map_type 1}
              color_map {
               [0.00 color rgbt <1,1,1,1>]
               [1.00 color rgbt <1,1,1,0>]
              }
             }
             finish {ambient 0.0
                     diffuse 0.85 }
            }
        }
    #end

    // This is to simulate diffusion of sunlight in the atmosphere:
    #if (use_diff=true)
        difference{
            sphere {0,EARTH_RADIUS*1.0037*diff_adj}
            sphere {0,EARTH_RADIUS*1.0003}

            hollow
            material {
                texture {
                 pigment {rgbf 1}
                    finish {ambient 0.0}
                }

                interior{
                    media {
                        scattering {4, <0.0005,0.0005,0.001>}
                        intervals 1

                    }
                }
            }
        }
    #end

    // This is to simulate the oblateness of the earth
    scale <1, 0.9967, 1>
    rotate y*axial_rot
    rotate x*-23.4
}
#end
