//* Generated on : 03-Feb-2020 14:41:29

#version 3.7;

global_settings {
    assumed_gamma 1.5
    noise_generator 2
}

#include "satellite.inc"
#include "earth.inc"


camera {
   perspective
   location <618.336,4248.8737,6597.3908>
   look_at <618.3281,4248.8672,6597.3912>
   right <1.6,0,0>
   angle 31.7187
}

object {Earth(108.4487,true,true,false,1.0)}

object {
    spacecraft
    scale <0.001,0.001,0.001>
    matrix <-0.44435,0.56175,-0.69785,
            -0.55797,0.4359,0.70616,
            0.70088,0.70316,0.11975,
            618.3281,4248.8672,6597.3912>
}

light_source {
   <1.4723e8,0,0>, rgb<1,1,1>
   area_light
   <696340,0,0>, <0,696340,0>, 32, 32
   adaptive 0
   jitter
   circular
   orient
}

